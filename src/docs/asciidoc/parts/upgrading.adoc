== Upgrading From Older Releases

=== v2.0

Sources are now genreated into the `./.generated-src/gradleTestPlugin` directory instead of `build/gradleTest/src`. This si so that IntelliJ can correctly identify the test source code.

=== v1.x

Any version of Gradle 2.x listed in `gradleTest.versions` will be ignored during testing. A warning message will be printed in this case.

=== v0.5.x

`%%.VERSION%%` is no longer supported when building with Gradle 2.13+ and builds will fail with
`Could not find foo:bar:%%VERSION%%`` error if encountered. Simply remove the classpath, and if
you don't anything extra in the `buildscript` block, the whole of the `buildscript` block.

For instance the following block can be completely removed:

[source,groovy]
----
buildscript {
    dependencies {
        classpath 'com.github.jruby-gradle:jruby-gradle-plugin:%%VERSION%%'
    }
}
----

