/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2015 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import org.gradle.api.DefaultTask
import org.gradle.api.Project
import org.gradle.api.artifacts.ConfigurationContainer
import org.gradle.api.file.Directory
import org.gradle.api.file.FileCollection
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputFiles
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.SourceSetContainer
import org.gradle.api.tasks.TaskAction
import org.ysb33r.gradle.gradletest.internal.GradleTestTaskFamilyConfiguration
import org.ysb33r.gradle.gradletest.internal.GradleVersions
import org.ysb33r.grolifant.api.core.ProjectOperations
import org.ysb33r.grolifant.api.v4.PropertyStore

import static org.ysb33r.gradle.gradletest.TestSet.getManifestDir
import static org.ysb33r.gradle.gradletest.internal.GradleTestTaskFamilyConfiguration.*
import static org.ysb33r.gradle.gradletest.internal.GradleVersions.GRADLE_4_2_OR_LATER
import static org.ysb33r.gradle.gradletest.internal.GradleVersions.GRADLE_4_3_OR_LATER

/** Generates a manifest file that can be re-used by @link GradleTest tasks to set the correct classpath
 * for {@code GradleRunnerSteps}.
 *
 * @since 1.0
 */
@CompileStatic
class ClasspathManifest extends DefaultTask {

    /** Name of the test task this is linked to.
     * Under normal circumstances this property should not be modified by a build script author
     */
    @Input
    String linkedTestTaskName = name.replaceAll(Names.MANIFEST_TASK_POSTFIX, '')

    /** Name of the additional configuration this is linked to.
     * Under normal circumstances this property should not be modified by a build script author
     */
    @Input
    String linkedConfigurationName = name.replaceAll(Names.MANIFEST_TASK_POSTFIX, '')

    /** Name of the source set fomr which dependencies will be obtained.
     * Under normal circumstances this property should not be modified by a build script author.
     * This can be set to {@code null} in which case no source set will be used
     */
    @Optional
    @Input
    String sourceSetName = 'main'

    ClasspathManifest() {
        this.grolifant = ProjectOperations.find(project)
        this.projectDir = project.layout.projectDirectory
        this.outputDir = PropertyStore.create(File, project)
        this.configurations = project.configurations
        this.sourceSets = getSourceSetContainer(project)

        if (GRADLE_4_3_OR_LATER) {
            this.outputDir.set(getManifestDir(project.layout, linkedTestTaskName))
        } else {
            this.outputDir.set(getManifestDir(project, linkedTestTaskName))
        }
    }

    /** Returns directory where manifest file will be written to.
     *
     * @return
     */
    @OutputDirectory
    File getOutputDir() {
        this.outputDir.get()
    }

    /** The name of the manifest file.
     *
     * @return Filename as a string.
     */
    @Input
    String getOutputFilename() {
        linkedTestTaskName + '-manifest.txt'
    }

    /** Returns the runtime classpath associated with this manifest.
     *
     * @return Runtime classpath
     */
    @InputFiles
    FileCollection getRuntimeClasspath() {
        if (sourceSetName) {
            sourceSet.runtimeClasspath + configurations.getByName(linkedConfigurationName)
        } else {
            configurations.getByName(linkedConfigurationName)
        }
    }

    @TaskAction
    void exec() {
        File dest = getOutputDir()
        dest.mkdirs()
        outputFile("${dest}/${outputFilename}").text = runtimeClasspath.join("\n")
    }

    @CompileDynamic
    private File outputFile(String filename) {
        if (GRADLE_4_2_OR_LATER) {
            projectDir.file(filename).asFile
        } else {
            projectDir.file(filename).get()
        }
    }

    private SourceSet getSourceSet() {
        sourceSets.getByName(sourceSetName)
    }

    private final PropertyStore<File> outputDir
    private final ConfigurationContainer configurations
    private final Directory projectDir
    private final ProjectOperations grolifant
    private final SourceSetContainer sourceSets
}
