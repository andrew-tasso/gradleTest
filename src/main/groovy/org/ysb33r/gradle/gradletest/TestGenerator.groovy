/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2015 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest

import groovy.io.FileType
import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.DefaultTask
import org.gradle.api.DomainObjectSet
import org.gradle.api.GradleException
import org.gradle.api.Transformer
import org.gradle.api.artifacts.ConfigurationContainer
import org.gradle.api.artifacts.ExternalModuleDependency
import org.gradle.api.artifacts.ProjectDependency
import org.gradle.api.artifacts.UnknownConfigurationException
import org.gradle.api.file.CopySpec
import org.gradle.api.file.SourceDirectorySet
import org.gradle.api.provider.Provider
import org.gradle.api.provider.ProviderFactory
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputDirectory
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.SourceSetContainer
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.TaskContainer
import org.gradle.api.tasks.bundling.Jar
import org.gradle.util.GradleVersion
import org.ysb33r.gradle.gradletest.internal.TestKitLocations
import org.ysb33r.gradle.gradletest.internal.TestPreparation
import org.ysb33r.grolifant.api.core.ProjectOperations
import org.ysb33r.grolifant.api.v4.FileUtils
import org.ysb33r.grolifant.api.v4.PropertyStore
import org.ysb33r.grolifant.api.v4.StringUtils

import java.nio.file.Paths
import java.util.regex.Pattern

import static org.ysb33r.gradle.gradletest.GradleScriptLanguage.GROOVY
import static org.ysb33r.gradle.gradletest.GradleScriptLanguage.KOTLIN
import static org.ysb33r.gradle.gradletest.Names.GENERATOR_TASK_POSTFIX
import static org.ysb33r.gradle.gradletest.TestSet.getManifestTaskName
import static org.ysb33r.gradle.gradletest.internal.GradleTestTaskFamilyConfiguration.getSourceSetContainer
import static org.ysb33r.gradle.gradletest.internal.TaskUtils.named
import static org.ysb33r.grolifant.api.core.LegacyLevel.PRE_5_6

/** Generates test files that will be compiled against GradleTestKit.
 *
 * @since 1.0
 */
@CompileStatic
class TestGenerator extends DefaultTask {
    public final static GradleVersion MINIMUM_SUPPORTED_GRADLE_VERSION_FOR_KOTLIN_DSL = GradleVersion.version('4.10')

    TestGenerator() {
        def tasks = project.tasks
        onlyIf { getTestRootDirectory().exists() }

        this.grolifant = ProjectOperations.find(project)
        this.configurations = project.configurations
        this.providerFactory = project.providers
        this.sourceSets = getSourceSetContainer(project)
        this.linkedTestTask = java.util.Optional.of((Object) name.replaceAll(GENERATOR_TASK_POSTFIX, ''))

        linkedTestTaskNameProvider = project.provider({ java.util.Optional<Object> s ->
            StringUtils.stringize(s.get())
        }.curry(this.linkedTestTask))

        linkedTestTaskProvider = grolifant.map(linkedTestTaskNameProvider, { String it ->
            named(tasks, it, GradleTest)
        } as Transformer<GradleTest, String>)

        classpathManifestProvider = project.provider { ->
            named(tasks, getManifestTaskName(linkedTestTaskName), ClasspathManifest)
        }

        workDirProvider = grolifant.map(linkedTestTaskNameProvider, { String taskName ->
            grolifant.buildDirDescendant(taskName).get()
        } as Transformer<File, String>)

        pluginJarDirProvider = PropertyStore.create(File, project)
        pluginJarDirProvider.set(providerFactory.provider(PRE_5_6 ? jarDirPre56(tasks) : jarDir(tasks)))

        this.testProjectSourceDir = PropertyStore.create(File, project)
        this.testProjectSourceDir.set(providerFactory.provider(
            { -> grolifant.file("src/${linkedTestTaskName}")
            }))

        this.testPackageNameProvider = grolifant.map(linkedTestTaskNameProvider, { String it ->
            'compatibilityTests'
        } as Transformer<String, String>)
    }


    /**
     * Name of the test task this is linked to.
     * Under normal circumstances this property should not be modified by a build script author
     *
     * @return Name of linked test task
     */
    @Internal
    String getLinkedTestTaskName() {
        linkedTestTaskNameProvider.get()
    }

    /**
     * Provider to the name of the test task this is linked to.
     * @return Name provider.
     */
    @Internal
    Provider<String> getLinkedTestTaskNameProvider() {
        this.linkedTestTaskNameProvider
    }

    /** Toplevel name of the package tests will be placed within.
     * Under normal circumstances this property should not be modified by a build script author
     */
    @Internal
    Provider<String> getTestPackageName() {
        this.testPackageNameProvider
    }

    /** The Gradle versions that test code will be generated for
     *
     * @return List of (valid) Gradle versions
     */
    @Input
    Set<String> getVersions() {
        linkedTask.versions.get()
    }

    /** The Gradle versions that test code will be generated for
     *
     * @return List of (valid) Gradle versions
     *
     * @since 3.0
     */
    @Internal
    Provider<Set<String>> getVersionsProvider() {
        linkedTask.versions
    }

    /** List of arguments that needs to be passed to TestKit.
     *
     * @return List of arguments in order as passed to linked @link #GradleTest task.
     */
    @Input
    List<String> getGradleArguments() {
        linkedTask.gradleArguments
    }

    /** The default task to be executed.
     *
     * @return Default task as obtained from linked @link #GradleTest task.
     */
    @Input
    String getDefaultTask() {
        linkedTask.defaultTask
    }

    /** Whether to treat Gradle's deprecation messages as failures.
     *
     * @return {@code true} if deprecation messages should fail the tests.
     */
    @Input
    boolean getDeprecationMessageAreFailures() {
        linkedTask.getDeprecationMessagesAreFailures()
    }

    /** Whether to use the metadata generated by the {@code java-gradle-plugin}.
     *
     * @return {@code true} if deprecation messages should fail the tests.
     */
    @Input
    boolean getUsePluginUnderTestMetadata() {
        linkedTask.getUsePluginUnderTestMetadata()
    }

    /** Whether to add tests for Kotlin scripts if they are available.
     *
     * @return {@code true} if Kotlin scripts should be tested.
     */
    @Input
    boolean getKotlinDsl() {
        linkedTask.kotlinDsl
    }

    /** The root directory where to find tests for this specific GradleTest grouping
     * The default root directory by convention is {@code src/gradleTest}. THe patterns for the
     * directory is {@code src/} + {@code gradleTestSetName}.
     *
     * @return The directory as a file object resolved as per {@code project.file ( )}.
     */
    @InputDirectory
    File getTestRootDirectory() {
        this.testProjectSourceDir.get()
    }

    /**
     * Overrides thy default location of test project sources.
     *
     * @param src The location of the gradle test project sources.
     *
     * @since 3.0
     */
    void setTestRootDirectory(Object src) {
        grolifant.updateFileProperty(this.testProjectSourceDir, src)
    }

    /** A map of the tests found in the appropriate GradleTest directory.
     * The default root directory by convention. See @link #getTestRootDirectory().
     * Locates all folder below the root which contains a {@code build.gradle} file -
     * other folders are ignored. If {@code getKotlinDsl ( )} returns {@code true}, then
     * folders containing {@code build.gradle.kts} will also be included
     *
     * @return A map of consisting of {@code <TestName,PathToTest>}.
     */
    @Input
    TreeMap<String, TestDefinition> getTestMap() {
        TreeMap<String, TestDefinition> derivedTestNames = [:] as TreeMap
        final File root = testRootDirectory
        if (root.exists()) {
            def excludeFiles = [GROOVY.settingsFilePattern, KOTLIN.settingsFilePattern]
            // define the file name filter to find one or more build files
            def groovyFilter = new FilenameFilter() {
                boolean accept(File path, String filename) {
                    boolean isFile = Paths.get(path.absolutePath, filename).toFile().isFile()
                    return (filename.endsWith(GROOVY.buildExtPattern) && !(filename in excludeFiles) && isFile)
                }
            }

            def kotlinFilter = new FilenameFilter() {
                boolean accept(File path, String filename) {
                    boolean isFile = Paths.get(path.absolutePath, filename).toFile().isFile()
                    return (filename.endsWith(KOTLIN.buildExtPattern) && !(filename in excludeFiles) && isFile)
                }
            }

            root.eachFile(FileType.DIRECTORIES) { File dir ->
                List<File> groovyBuildFiles = dir.listFiles(groovyFilter) as List
                List<File> kotlinBuildFiles = dir.listFiles(kotlinFilter) as List

                if ((groovyBuildFiles.size() > 0) || (kotlinBuildFiles.size() > 0)) {
                    derivedTestNames[dir.name] = new TestDefinition(dir.canonicalFile, groovyBuildFiles, kotlinBuildFiles)
                }
            }
        }
        derivedTestNames
    }

    /** Where generated source is written to.
     *
     * @return Output directory.
     */
    @OutputDirectory
    File getOutputDir() {
        sourceDirectorySet.srcDirs.first()
    }

    /** The directory where the plugin JAR is to be found. By default it will be {@code jar.destinationDir}
     *
     * @return Plugin directory.
     */
    @Internal
    File getPluginJarDirectory() {
        pluginJarDirProvider.get()
    }

    /** Overrides the directory where the plugin JAR is to be found in.
     *
     * @param dir Sets a new location
     * @return
     */
    void setPluginJarDirectory(Object dir) {
        pluginJarDirProvider.set(providerFactory.provider({ ->
            grolifant.file(dir)
        }))
    }

    /** Distribution URI to use when looking for Gradle distributions
     *
     * @return Distribution URI or null (indicating to use official Gradle repository).
     */
    @Input
    @Optional
    URI getGradleDistributionUri() {
        linkedTask.gradleDistributionUri
    }

    @Input
    String getGradleFilenamePattern() {
        linkedTask.gradleDistributionFilenamePattern
    }

    /** Whether configuration cache should be used in tests where this feasture is available.
     *
     * @return {@code true} when this feature should be used.
     *
     * @since 3.0
     */
    @Input
    String getWithConfigurationCache() {
        linkedTask.withConfigurationCache.mode
    }

    /** Task action will generate tests as per the testnames returned by {@link #getTestMap()}.
     *
     */
    @TaskAction
    void exec() {
        if (!templateFile) {
            setTemplateLocationFromResource()
        }

        if (!templateInitScript) {
            setInitScriptLocationFromResource()
        }

        final ClasspathManifest manifestTask = classpathManifestProvider.get()
        final File manifestDir = manifestTask.outputDir
        final File manifestFile = new File("${manifestDir}/${manifestTask.outputFilename}")
        final File workDir = workDirProvider.get()
        final File repoDir = new File(workDir, 'repo')
        final List<Pattern> testPatternsForFailures = getLinkedTask().getExpectedFailures()
        final boolean deprecation = getDeprecationMessageAreFailures()
        final boolean withDebug = getLinkedTask().debugTests
        final boolean cleanCache = getLinkedTask().cleanCache

        final TestKitLocations testKitLocations = getLinkedTask().testKitStrategy

        Set<File> externalDependencies = []
        try {
            DomainObjectSet<ExternalModuleDependency> externalDependencySet = configurations.getByName('runtimeClasspath')
                .allDependencies
                .withType(ExternalModuleDependency)
            DomainObjectSet<ProjectDependency> projectDependencySet = configurations.getByName('runtimeClasspath')
                .allDependencies
                .withType(ProjectDependency)
            externalDependencies = configurations.getByName('runtimeClasspath').files { dep ->
                externalDependencySet.contains(dep) || projectDependencySet.contains(dep)
            }
        } catch (UnknownConfigurationException e) {
        }

        createInitScript(workDir, pluginJarDirectory, repoDir, externalDependencies)

        // Before generation delete existing generated code
        outputDir.deleteDir()

        testMap.each { String testName, TestDefinition testDef ->
            boolean expectFailure = testPatternsForFailures.find { pat ->
                testName =~ pat
            }

            copy(
                new TestPreparation(
                    targetDir: outputDir,
                    testBase: testName,
                    defaultTask: defaultTask,
                    manifestFile: usePluginUnderTestMetadata ? null : manifestFile,
                    workDir: workDir,
                    testDefinitions: testDef,
                    willFail: expectFailure,
                    deprecationMessageMode: deprecation,
                    withDebug: withDebug,
                    cleanCache: cleanCache,
                    testKitLocations: testKitLocations,
                    configurationCache: withConfigurationCache
                ),
                gradleArguments
            )
        }

        createRepo(repoDir)
    }

    /** Created a local repo for use by tests.
     * it will copy from the appropriate configuration i.e. {@code gradleTest}, {@code fooGradleTest}} etc.
     *
     * @param repoDir Where to copy files to
     */
    private void createRepo(final File repoDir) {
        grolifant.copy({ ConfigurationContainer cc, CopySpec cs ->
            cs.from(cc.getByName(linkedTestTaskName))
            cs.into(repoDir)
        }.curry(configurations) as Action<CopySpec>)
    }

    /** Creates a init script to be used for running tests
     *
     * @param targetDir Where the init script is copied to
     * @param jarDir The path where the plugin JAR will be found
     * @param repoDir The path to where the local repo will be created
     * @param externalDependencies A list of external dependencies (JARs).
     */
    @CompileDynamic
    private void createInitScript(
        final File targetDir, final File jarDir, final File repoDir, Set<File> externalDependencies) {
        final def fromSource = templateInitScript
        final String pluginJarPath = pathAsUriStr(jarDir)
        final String repoPath = pathAsUriStr(repoDir)
        final String externalDepPaths = externalDependencies.collect {
            "'${pathAsUriStr(it)}'.toURI()"
        }.join(',')

        def gradleTask = getLinkedTask()

        grolifant.copy { CopySpec cs ->
            cs.with {
                from fromSource
                into targetDir
                expand PLUGINJARPATH: pluginJarPath,
                    LOCALREPOPATH: repoPath,
                    EXTERNAL_DEP_URILIST: externalDepPaths,
                    BUILDSCRIPT_DEPENDENCY_BLOCK: gradleTask.initBuildscriptRepositoryBlock
            }
        }
    }

    /** Get linked test as a @link #GradleTest object.
     *
     * @return @link #GradleTest object.
     */
    private GradleTest getLinkedTask() {
        linkedTestTaskProvider.get()
    }

    /** Finds the Groovy source directory set associated with this GradleTest set.
     *
     * @return {@code SourceDirectorySet}
     */
    @CompileDynamic
    private SourceDirectorySet getSourceDirectorySet() {
        sourceSets.getByName(linkedTestTaskName).getGroovy()
    }

    /** Processes the groovy and kotlin arrays to generate the Spock files
     *
     * @param testPrep Attributes for preparing the test source.
     * @param arguments Arguments that will be passed during a run.
     */
    private void copy(
        final TestPreparation testPrep,
        final List<String> arguments
    ) {
        copySub(testPrep.testDefinitions.groovyBuildFiles,
            GROOVY,
            testPrep,
            cloneList(arguments),
        )

        copySub(testPrep.testDefinitions.kotlinBuildFiles,
            KOTLIN,
            testPrep,
            cloneList(arguments),
        )

    }

    private List<String> cloneList(List<String> list) {
        List<String> clone = []
        clone.addAll(list)
        clone
    }

    private void copySub(
        List<File> buildFiles,
        final GradleScriptLanguage language,
        final TestPreparation testPrep,
        final List<String> arguments
    ) {

        if (language == KOTLIN && testPrep.withDebug) {
            logger.warn "Tests for Kotlin DSL based scripts will not run in debug mode due to https://github.com/gradle/kotlin-dsl/issues/1261"
        }

        for (File buildFile in buildFiles) {
            final String testName = "${testPrep.testBase}${testClassifierName(buildFile)}"

            copyWorker(
                testPrep,
                testName,
                buildFile.name,
                language,
                arguments
            )
        }
    }

    private String testClassifierName(final File buildFile) {
        final String buildFileName = buildFile.name
        if (buildFileName == GROOVY.buildFilePattern || buildFileName == KOTLIN.buildFilePattern) {
            ''
        } else {
            ':' + buildFileName.split("\\.")[0]
        }
    }

    private void copyWorker(
        final TestPreparation testPrep,
        final String testName,
        final String testScript,
        final GradleScriptLanguage language,
        final List<String> arguments
    ) {

        final boolean isKotlinTest = language == KOTLIN
        final Object fromSource = templateFile
        final String verText = quoteAndJoin(isKotlinTest ? kotlinDslSafeVersions() : versionsProvider.get())
        final String argsText = quoteAndJoin([testPrep.defaultTask] + arguments)
        final String manifest = testPrep.manifestFile ? pathAsUriStr(testPrep.manifestFile) : '.'
        final String work = pathAsUriStr(testPrep.workDir)
        final String src = pathAsUriStr(testPrep.testDefinitions.testDir)
        final String deprecation = testPrep.deprecationMessageMode.toString()
        final String deleteScript = isKotlinTest ? GROOVY.buildFilePattern : KOTLIN.buildFilePattern
        final String testKitLoc = testKitLocationString(testPrep.testKitLocations)
        final String testPackageNameFinalised = "${testPackageName.get()}.${language.id}lang"
        final String testPackagePath = testPackageNameFinalised.replaceAll('\\.', '/')
        final String testClassFileName = "${testName.capitalize().replaceAll(/\W/, '_')}Spec.groovy"
        final String buildClasspath = testPrep.manifestFile ? 'true' : 'false'

        if (verText.empty) {
            logger.warn("Kotlin DSL testing has been enabled, but none of the specified Gradle versions are ${MINIMUM_SUPPORTED_GRADLE_VERSION_FOR_KOTLIN_DSL} or later.")
            return
        }

        grolifant.copy { CopySpec cs ->
            cs.with {
                from fromSource
                into new File(testPrep.targetDir, testPackagePath)
                rename ~/.+/, testClassFileName
                expand TESTPACKAGE: testPackageNameFinalised,
                    TESTNAME: testName.capitalize(),
                    TESTBASENAME: testPrep.testBase,
                    TESTFILENAME: testScript,
                    LANGUAGE: language.id,
                    MANIFEST: manifest,
                    ARGUMENTS: argsText,
                    DEFAULTTASK: testPrep.defaultTask,
                    VERSIONS: verText,
                    DISTRIBUTION_URI: gradleDistributionUri ?: '',
                    WORKDIR: work,
                    SOURCEDIR: src,
                    FAILMODE: testPrep.willFail,
                    CHECK_WARNINGS: deprecation,
                    DELETE_SCRIPT: deleteScript,
                    TESTKIT_PER_TEST: testKitLoc,
                    DIST_FILENAME_PATTERN: gradleFilenamePattern,
                    // See Gradle bug https://github.com/gradle/kotlin-dsl/issues/1261
                    // See Gradle bug https://github.com/gradle/gradle/issues/6862
                    WITH_DEBUG: isKotlinTest ? false : testPrep.withDebug,
                    CLEAN_CACHE: testPrep.cleanCache,
                    BUILD_PLUGIN_CLASS_PATH: buildClasspath,
                    CONFIGURATION_CACHE: testPrep.configurationCache
            }
        }
    }

    /** Create a string suitable for sending to the Spock template.
     *
     * @param testKitLocation
     * @return
     */
    private String testKitLocationString(TestKitLocations testKitLocation) {
        String testKitLoc = 'null'
        switch (testKitLocation) {
            case TestKitLocations.PER_TEST:
                testKitLoc = 'true'
                break
            case TestKitLocations.PER_GROUP:
                testKitLoc = 'false'
                break
        }
        testKitLoc
    }

    /** Quotes items in non-interpolated strings amd then joins them as a comma-separated list
     *
     * @param c Any container
     * @return Comma-separated list
     */
    @CompileDynamic
    private String quoteAndJoin(Iterable c) {
        c.collect { "'${it}'" }.join(',')
    }

    /** Returns versions which are safe for Kotlin testing
     *
     * @return versions for which Kotlin DSL can be tested.
     */
    private Iterable<String> kotlinDslSafeVersions() {
        versionsProvider.get().findAll {
            GradleVersion.version(it) >= MINIMUM_SUPPORTED_GRADLE_VERSION_FOR_KOTLIN_DSL
        }
    }

    /** Ensures that files are represented as URIs.
     * (This helps with compatibility across operating systems).
     * @param path Path to output
     */
    String pathAsUriStr(final File path) {
        path.absoluteFile.toURI().toString()
    }

    /** Finds the template in the classpath.
     *
     */
    private void setTemplateLocationFromResource() {
        this.templateFile = getLocationFromResource(
            TEST_TEMPLATE_PATH_JUNIT5
        )
    }

    /** Finds the init script template in the classpath.
     *
     */
    private void setInitScriptLocationFromResource() {
        this.templateInitScript = getLocationFromResource(INIT_TEMPLATE_PATH)
    }

    private def getLocationFromResource(final String resourcePath) {
        Enumeration<URL> enumResources
        String resourceName = new File(resourcePath).name
        enumResources = this.class.classLoader.getResources(resourcePath)
        if (!enumResources.hasMoreElements()) {
            throw new GradleException("Cannot find ${resourcePath} in classpath")
        }

        URI uri = enumResources.nextElement().toURI()
        String location = uri.getSchemeSpecificPart().replace('!/' + resourcePath, '')
        if (uri.scheme.startsWith('jar')) {
            location = location.replace('jar:file:', '')
            return grolifant.zipTree(location).filter { File it -> it.name == resourceName }
        } else if (uri.scheme.startsWith('file')) {
            return location.replace('file:', '')
        }

        throw new GradleException("Cannot extract ${uri}")
    }

    private static Closure jarDirPre56(TaskContainer tasks) {
        // Although getDestinationDirectory exists in 5.1 already,
        // the lack of an interface called FileSystemLocationProperty in <5.6
        // will cause jarDir to break.
        return { ->
            named(tasks, 'jar', Jar).destinationDir
        }
    }

    private static Closure jarDir(TaskContainer tasks) {
        return { ->
            tasks.named('jar', Jar).get().destinationDirectory.asFile.get()
        }
    }

    private def templateFile
    private def templateInitScript

    private final java.util.Optional<Object> linkedTestTask
    private final Provider<String> linkedTestTaskNameProvider

    private final Provider<GradleTest> linkedTestTaskProvider
    private final PropertyStore<File> pluginJarDirProvider
    private final ProjectOperations grolifant
    private final ConfigurationContainer configurations
    private final SourceSetContainer sourceSets
    private final ProviderFactory providerFactory
    private final Provider<ClasspathManifest> classpathManifestProvider
    private final Provider<File> workDirProvider
    private final Provider<String> testPackageNameProvider
    private final PropertyStore<File> testProjectSourceDir
//    private final Provider<Set<File>> externalDependencyProvider
//    private final Configuration runtimeClasspath

    public static final String TEST_TEMPLATE_PATH_JUNIT5 = 'org/ysb33r/gradletest/GradleTestTemplate.groovy.template'
    public static final String INIT_TEMPLATE_PATH = 'org/ysb33r/gradletest/init.gradle'
}
