/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2015 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import groovy.transform.PackageScope
import groovy.transform.TypeChecked
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.Transformer
import org.gradle.api.file.Directory
import org.gradle.api.file.ProjectLayout
import org.gradle.api.file.SourceDirectorySet
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.TaskContainer
import org.gradle.api.tasks.compile.GroovyCompile
import org.ysb33r.grolifant.api.errors.NotSupportedException

import static org.gradle.language.base.plugins.LifecycleBasePlugin.CHECK_TASK_NAME
import static org.ysb33r.gradle.gradletest.internal.GradleTestTaskFamilyConfiguration.configureGenTask
import static org.ysb33r.gradle.gradletest.internal.GradleTestTaskFamilyConfiguration.configureGradleTestTask
import static org.ysb33r.gradle.gradletest.internal.GradleTestTaskFamilyConfiguration.configureGroovyCompileTask
import static org.ysb33r.gradle.gradletest.internal.GradleTestTaskFamilyConfiguration.configureManifestTask
import static org.ysb33r.gradle.gradletest.internal.GradleTestTaskFamilyConfiguration.getSourceSetContainer
import static org.ysb33r.grolifant.api.core.LegacyLevel.PRE_4_1
import static org.ysb33r.grolifant.api.core.LegacyLevel.PRE_4_10
import static org.ysb33r.grolifant.api.core.LegacyLevel.PRE_4_3
import static org.ysb33r.grolifant.api.core.LegacyLevel.PRE_5_0

/** Internal utility functions to add a new GradleTest test set.
 *
 * @since 1.0
 */
@CompileStatic
class TestSet {

    public final static String SPOCK_V2_VERSION = '2.0-M5'
    public final static String SPOCK_V1_VERSION = '1.3'
    public final static String JUNIT_VERSION = '4.12'
    public final static String HAMCREST_VERSION = '1.3'

    /** Adds a test set with default dependencies
     *
     * @param project Project to add tests to
     * @param testSetName Name of test set
     */
    static void addTestSet(Project project, final String testSetName) {
        String setname = baseName(testSetName)
        project.configurations.maybeCreate setname
        addSourceSets(project, setname)
        addTestTasks(project, setname)
        addManifestTask(project, setname)
        addTestDependencies(project, setname)

        disableLicenseSourceChecks(project, setname)
    }

    /** Get a testset base name
     *
     * @param name Name to use as seed
     * @return Name that will be used as basename for configrations and tasks
     */
    static String baseName(final String testSetName) {
        testSetName == Names.DEFAULT_TASK ? Names.DEFAULT_TASK : "${testSetName}GradleTest"
    }

    /** Gets the source dir where test code will be generated into.
     *
     * @param project
     * @param testSetBaseName A basename as returned by @link #baseName
     * @return A closure that can be lazy-evaluated.
     */
    @PackageScope
    static Closure getSourceDir(Project project, final String testSetBaseName) {
        { Project p, final String setname, final String postfix ->
            "${p.projectDir}/.generated-src/gradleTestPlugin/${setname}/src${postfix}"
        }.curry(project, testSetBaseName, PRE_4_1 ? '' : "/${LANG_NAME}")
    }

    /** Gets the directory where manifest file will be generated into.
     *
     * @param project
     * @param testSetBaseName A basename as returned by @link #baseName
     * @return A closure that can be lazy-evaluated.
     * @deprecated Only call this from Gradle < 4.3.
     */
    @Deprecated
    @PackageScope
    static Provider<File> getManifestDir(Project project, final String testSetBaseName) {
        if (PRE_4_3) {
            project.provider({ ->
                new File(project.buildDir, "${testSetBaseName}/manifest")
            })
        } else {
            throw new NotSupportedException('This method cannot not be called for Gradle 4.3+')
        }
    }

    /** Gets the directory where manifest file will be generated into.
     *
     * @param project
     * @param testSetBaseName A basename as returned by @link #baseName
     * @return A closure that can be lazy-evaluated.
     */
    @PackageScope
    static Provider<File> getManifestDir(ProjectLayout project, final String testSetBaseName) {
        String path = "${testSetBaseName}/manifest"
        if (PRE_4_3) {
            throw new NotSupportedException('This method cannot not be called for Gradle < 4.3')
        } else {
            project.buildDirectory.dir(path).map({ Directory it ->
                it.asFile
            } as Transformer<File, Directory>)
        }
    }

    /** Gets the resource directory where test resources will be generated into.
     *
     * @param project
     * @param testSetBaseName A basename as returned by @link #baseName
     * @return A closure that can be lazy-evaluated.
     */
    @PackageScope
    @Deprecated
    static Provider<File> getResourcesDir(Project project, final String testSetBaseName) {
        if (PRE_4_3) {
            project.provider({ ->
                new File(project.buildDir, "${testSetBaseName}/resources")
            })
        } else {
            throw new NotSupportedException('This method cannot not be called for Gradle 4.3+')
        }
    }

    /** Gets the resource directory where test resources will be generated into.
     *
     * @param project
     * @param testSetBaseName A basename as returned by @link #baseName
     * @return A closure that can be lazy-evaluated.
     */
    @PackageScope
    static Provider<File> getResourcesDir(ProjectLayout project, final String testSetBaseName) {
        if (PRE_4_3) {
            throw new NotSupportedException('This method cannot not be called for Gradle < 4.3')
        } else {
            project.buildDirectory.dir("${testSetBaseName}/resources").map({ it.asFile })
        }
    }

    @PackageScope
    static String getGeneratorTaskName(final String testSetBaseName) {
        "${testSetBaseName}${Names.GENERATOR_TASK_POSTFIX}"
    }

    @PackageScope
    static String getLicenseTaskName(final String testSetBaseName) {
        "license${testSetBaseName.capitalize()}"
    }

    @PackageScope
    static String getCodenarcTaskName(final String testSetBaseName) {
        "codenarc${testSetBaseName.capitalize()}"
    }

    @PackageScope
    static String getTestTaskName(final String testSetBaseName) {
        testSetBaseName
    }

    static String getManifestTaskName(final String testSetBaseName) {
        "${testSetBaseName}${Names.MANIFEST_TASK_POSTFIX}"
    }

    /** Adds the required test dependencies to a configuration
     *
     * @param project
     * @param configurationName
     */
    @PackageScope
    static void addTestDependencies(Project project, String testSetBaseName) {
        final SourceSet sourceSet = getSourceSetContainer(project).getByName(testSetBaseName)
        final String configurationName = sourceSet.implementationConfigurationName
        project.dependencies.with {
            add(configurationName, gradleTestKit())
            add(configurationName, gradleApi())
        }

        addSpockDependencyTo(project, configurationName)

        if (spockMode == SPOCK_V1_VERSION) {
            project.dependencies.with {
                add configurationName, "junit:junit:${JUNIT_VERSION}"
                add configurationName, "org.hamcrest:hamcrest-core:${HAMCREST_VERSION}"
            }
        }
    }

    @PackageScope
    static void addSourceSets(Project project, final String testSetBaseName) {
        String[] includeGlobs = System.getProperty("${testSetBaseName}.include")?.split(',')
        String[] excludeGlobs = System.getProperty("${testSetBaseName}.exclude")?.split(',')

        SourceSet ss = getSourceSetContainer(project).create(testSetBaseName)
        SourceDirectorySet groovy = groovySourceDirectorySet(ss)

        groovy.with {
            srcDirs = [project.file(getSourceDir(project, testSetBaseName))]

            if(includeGlobs) {
                include(includeGlobs)
            }

            if(excludeGlobs) {
                exclude(excludeGlobs)
            }
        }

        ss.resources.with {
            srcDirs = [
                project.file(PRE_4_3 ?
                    getResourcesDir(project, testSetBaseName) :
                    getResourcesDir(project.layout, testSetBaseName)
                )
            ]
        }
    }

     @PackageScope
    static void addManifestTask(Project project, final String testSetBaseName) {
        if (PRE_4_10) {
            createManifestTask(project.tasks, testSetBaseName)
        } else {
            registerManifestTask(project.tasks, testSetBaseName)
        }
    }

    @PackageScope
    static void addTestTasks(Project project, final String testSetBaseName) {

        final String genTaskName = getGeneratorTaskName(testSetBaseName)
        final String testTaskName = getTestTaskName(testSetBaseName)
        final String manifestTaskName = getManifestTaskName(testSetBaseName)
        final SourceSet sourceSet = getSourceSetContainer(project).getByName(testSetBaseName)

        if (PRE_4_10) {
            createTestTasks(
                project.tasks,
                sourceSet,
                testTaskName,
                genTaskName,
                manifestTaskName
            )
        } else {
            registerTestTasks(
                project.tasks,
                sourceSet,
                testTaskName,
                genTaskName,
                manifestTaskName
            )
        }

        configureCompileTask(project.tasks, sourceSet.getCompileTaskName(LANG_NAME)  ,genTaskName)
    }

    /**
     * Returns which version of Spock to use.
     *
     * @return {@link #SPOCK_V1_VERSION} or {@link #SPOCK_V2_VERSION}
     */
    static String getSpockMode() {
        final String gVer = GroovySystem.version
        String spockVer
        if (gVer.startsWith('3.') || gVer.startsWith('2.5.')) {
            spockVer = SPOCK_V2_VERSION
        } else if (gVer.startsWith('2.')) {
            spockVer = SPOCK_V1_VERSION
        } else {
            spockVer = SPOCK_V2_VERSION
        }
    }

    /** Creates a Spock Framework dependency
     *
     * @param project
     * @return
     */
    @PackageScope
    @CompileDynamic
    static void addSpockDependencyTo(Project project, String cfgName) {
        final String spockVer = spockMode
        final String spockFullVer = "${spockVer}-groovy-${GroovySystem.version.replaceAll(/\.\d+(-SNAPSHOT)?$/, '')}"

        switch (spockVer) {
            case SPOCK_V1_VERSION:
                project.dependencies.add(cfgName, "org.spockframework:spock-core:${spockFullVer}") {
                    exclude module: 'groovy-all'
                }
                break
            default:
                project.dependencies.add(cfgName, "org.spockframework:spock-core:${spockFullVer}") {
                    exclude module: 'groovy'
                    transitive = true
                }
        }

    }

    /** Disables license checking if the Hierynomus License plugin is applied.
     *
     * @param project
     * @param testSetBaseName
     */
    @PackageScope
    static void disableLicenseSourceChecks(Project project, final String testSetBaseName) {
        project.pluginManager.withPlugin('com.github.hierynomus.license') {
            project.tasks.getByName(getLicenseTaskName(testSetBaseName)).enabled = false
        }
    }

    @PackageScope
    static void disableCodenarcSourceChecks(Project project, final String testSetBaseName) {
        project.pluginManager.withPlugin('codenarc') {
            project.tasks.getByName(getCodenarcTaskName(testSetBaseName)).enabled = false
        }
    }

    @CompileDynamic
    @TypeChecked
    private static void registerTestTasks(
        TaskContainer tasks,
        SourceSet sourceSet,
        String testTaskName,
        String genTaskName,
        String manifestTaskName
    ) {
        def genTask = tasks.register(genTaskName, TestGenerator)
        def testTask = tasks.register(testTaskName, GradleTest)

        genTask.configure {
            configureGenTask(it)
        }

        testTask.configure {
            configureGradleTestTask(it, sourceSet, genTask, sourceSet.getCompileTaskName(LANG_NAME), manifestTaskName)
        }

        tasks.named(CHECK_TASK_NAME).configure {
            it.dependsOn(testTask)
        }
    }

    private static void createTestTasks(
        TaskContainer tasks,
        SourceSet sourceSet,
        String testTaskName,
        String genTaskName,
        String manifestTaskName
    ) {
        Task testTask = tasks.create(testTaskName, GradleTest)
        Task genTask = tasks.create(genTaskName, TestGenerator)
        configureGradleTestTask(testTask, sourceSet, genTask, sourceSet.getCompileTaskName(LANG_NAME), manifestTaskName)
        configureGenTask(genTask)
        tasks.getByName(CHECK_TASK_NAME).dependsOn testTask
    }

    @TypeChecked
    @CompileDynamic
    private static registerManifestTask(TaskContainer tasks, String testSetBaseName) {
        tasks.register(getManifestTaskName(testSetBaseName), ClasspathManifest).configure {
            configureManifestTask(it, testSetBaseName)
        }
    }

    @CompileDynamic
    @TypeChecked
    private static configureCompileTask(TaskContainer tasks, String compileTaskName, String genTaskName) {
        if (PRE_4_10) {
            configureGroovyCompileTask((GroovyCompile) tasks.getByName(compileTaskName), genTaskName)
        } else if(PRE_5_0) {
            tasks.named(compileTaskName).configure {
                configureGroovyCompileTask((GroovyCompile)it, genTaskName)
            }
        } else {
            tasks.named(compileTaskName, GroovyCompile) {
                configureGroovyCompileTask(it, genTaskName)
            }
        }
    }

    @CompileDynamic
    private static SourceDirectorySet groovySourceDirectorySet(SourceSet ss) {
        ss.groovy
    }


    private static createManifestTask(TaskContainer tasks, String testSetBaseName) {
        ClasspathManifest task = tasks.create(getManifestTaskName(testSetBaseName), ClasspathManifest)
        configureManifestTask(task, testSetBaseName)
    }

    private static final String LANG_NAME = 'groovy'
}
