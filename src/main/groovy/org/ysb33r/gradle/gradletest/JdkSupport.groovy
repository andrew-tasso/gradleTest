/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2015 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest

import groovy.transform.CompileStatic
import org.gradle.api.JavaVersion
import org.gradle.util.GradleVersion
import org.ysb33r.gradle.gradletest.internal.GradleVersions

import static org.ysb33r.gradle.gradletest.internal.GradleVersions.CURRENT
import static org.ysb33r.gradle.gradletest.internal.GradleVersions.MINIMUM_SUPPORTED

/**
 * Gradle JDK version support within the context of GradleTest.
 *
 * @author Schalk W. Cronjé
 *
 * @since 3.0
 */
@CompileStatic
class JdkSupport {

    /**
     * Mapping on Java version to minimum supported Gradle Version.
     *
     * @return The minimum supported version for the current Java version.
     *   Returns {@code null} is the java vesion is not mapped
     * @throws UnsupportedOperationException if JDK < 8.
     */
    static GradleVersion getMinimumGradleForCurrentJavaVersion() {
        Integer version = JavaVersion.current().majorVersion.toInteger()
        if(version < 8) {
            throw new UnsupportedOperationException("This version of GradleTest does not support JDK${version}")
        }
        VERSION_MAP[version] ?: CURRENT
    }

    private static final Map<Integer,GradleVersion> VERSION_MAP = [
        8: MINIMUM_SUPPORTED,
        9: GradleVersion.version('4.3'),
        10:GradleVersion.version('4.7'),
        11: GradleVersion.version('5.0'),
        12:GradleVersion.version('5.4'),
        13:GradleVersion.version('6.0'),
        14:GradleVersion.version('6.3'),
        15:GradleVersion.version('6.7'),
        16:GradleVersion.version('7.0'),
    ]

    private static final Integer MAX_KNOWN_JAVA_VERSION = VERSION_MAP.keySet().max()
}
