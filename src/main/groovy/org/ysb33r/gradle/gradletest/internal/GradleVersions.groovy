/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2015 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest.internal

import groovy.transform.CompileStatic
import org.gradle.util.GradleVersion

/** Fixed constants and methods for dealing with specific Gradle versions.
 *
 * @since 2.0
 */
@CompileStatic
class GradleVersions {


    public static final GradleVersion CURRENT = GradleVersion.current()
    public static final GradleVersion GRADLE_3_0 = GradleVersion.version('3.0')
    public static final GradleVersion GRADLE_4_0 = GradleVersion.version('4.0')
    public static final GradleVersion GRADLE_4_1 = GradleVersion.version('4.1')
    public static final GradleVersion GRADLE_4_2 = GradleVersion.version('4.2')
    public static final GradleVersion GRADLE_4_3 = GradleVersion.version('4.3')
    public static final GradleVersion GRADLE_4_5 = GradleVersion.version('4.5')
    public static final GradleVersion GRADLE_5_0 = GradleVersion.version('5.0')

    /**
     * Minimum version supported by GradleTest.
     *
     * @since 3.0
     */
    public static final GradleVersion MINIMUM_SUPPORTED = GRADLE_4_0

    /** Is Gradle 4.0 or later.
     *
     */
    @Deprecated
    public static final boolean GRADLE_4_0_OR_LATER = CURRENT >= GRADLE_4_0

    /** Is Gradle 4.1 or later.
     *
     */
    @Deprecated
    public static final boolean GRADLE_4_1_OR_LATER = CURRENT >= GRADLE_4_1

    /** Is Gradle 4.2 or later.
     *
     */
    @Deprecated
    public static final boolean GRADLE_4_2_OR_LATER = CURRENT >= GRADLE_4_2


    /** Is Gradle 4.1 or later.
     *
     */
    @Deprecated
    public static final boolean GRADLE_4_3_OR_LATER = CURRENT >= GRADLE_4_3

    /** Is Gradle 4.5 or later.
     *
     */
    @Deprecated
    public static final boolean GRADLE_4_5_OR_LATER = CURRENT >= GRADLE_4_5

    /** Is Gradle 5.0 or later.
     *
     */
    @Deprecated
    public static final boolean GRADLE_5_0_OR_LATER = CURRENT >= GRADLE_5_0
}
