/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2015 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest.internal

import groovy.transform.CompileStatic
import org.gradle.testing.jacoco.tasks.JacocoReportBase
import org.ysb33r.gradle.gradletest.GradleTest
import org.ysb33r.gradle.gradletest.TestGenerator

/** Configures GradleTest tasks for Jacoco.
 *
 * @since 2.0
 */
@CompileStatic
class Jacoco {

    /** Configures a GradleTest task to work with Jacoco.
     *
     * This should only be called during configuration phase.
     *
     * @param task GradleTest task to configure.
     */
    static void configure(final GradleTest task) {
        task.debugTests = true
        JacocoReportBase reporter = (JacocoReportBase) (task.project.tasks.getByName('jacocoTestReport'))
        reporter.executionData(task)
    }

    /** Conditionally configures a GradleTest task depending on the knowledge that the
     * associated TestGenerator task has about it.
     *
     * This should only be called during configuration phase.
     *
     * @param task TestGenerator task to query
     */
    static void configure(final TestGenerator task) {
        if (!task.testMap.isEmpty()) {
            configure((GradleTest) (task.project.tasks.getByName(task.linkedTestTaskName)))
        }
    }
}
