/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2015 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest.internal

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import groovy.transform.TypeChecked
import org.gradle.api.Project
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.SourceSetContainer
import org.gradle.api.tasks.compile.GroovyCompile
import org.ysb33r.gradle.gradletest.ClasspathManifest
import org.ysb33r.gradle.gradletest.GradleTest
import org.ysb33r.gradle.gradletest.Names
import org.ysb33r.gradle.gradletest.TestGenerator

import static org.ysb33r.gradle.gradletest.TestSet.SPOCK_V1_VERSION
import static org.ysb33r.gradle.gradletest.TestSet.getSpockMode
import static org.ysb33r.grolifant.api.core.LegacyLevel.PRE_4_1
import static org.ysb33r.grolifant.api.core.LegacyLevel.PRE_4_3
import static org.ysb33r.grolifant.api.core.LegacyLevel.PRE_5_0

/**
 * Utilities for configuration the GradleTest task.
 *
 * @author Schalk W. Cronjé
 *
 * @since 3.0
 */
@CompileStatic
class GradleTestTaskFamilyConfiguration {

    @CompileDynamic
    static SourceSetContainer getSourceSetContainer(Project project) {
        if(PRE_5_0) {
            (SourceSetContainer)project.sourceSets
        } else {
            project.extensions.getByType(SourceSetContainer)
        }
    }

    static void configureGradleTestTask(GradleTest testTask, SourceSet sourceSet, Object... dependsOnTasks) {
        testTask.with {
            group = Names.TASK_GROUP
            description = 'Runs Gradle compatibility tests'
            dependsOn dependsOnTasks
            mustRunAfter 'test'
            classpath = sourceSet.runtimeClasspath
        }
        setJunitMode(testTask)
        setClassesDirs(testTask,sourceSet)
    }

    static void configureGenTask(TestGenerator genTask) {
        genTask.with {
            group = 'build'
            description = 'Creates text fixtures for compatibility testing'
        }
    }

    static void configureManifestTask(ClasspathManifest classpathManifest, String testSetBaseName) {
        classpathManifest.with {
            group = Names.TASK_GROUP
            description = 'Creates manifest file for ' + testSetBaseName
        }
    }

    static void configureGroovyCompileTask(GroovyCompile task, Object... dependsOnTasks ) {
        task.with {
            dependsOn(dependsOnTasks)
            doFirst {
                destinationDir.deleteDir()
            }
        }
    }

    @CompileDynamic
    @TypeChecked
    private static void setJunitMode(GradleTest testTask) {
        if (spockMode == SPOCK_V1_VERSION) {
            testTask.useJUnit()
        } else {
            testTask.useJUnitPlatform()
        }
    }

    @CompileDynamic
    @TypeChecked
    private static void setClassesDirs(GradleTest testTask, SourceSet sourceSet) {
        if (PRE_4_1) {
            testTask.testClassesDir = sourceSet.output.classesDir
            testTask.inputs.dir(sourceSet.output.classesDir).skipWhenEmpty()
        } else {
            testTask.testClassesDirs = sourceSet.output.classesDirs
            testTask.inputs.files(sourceSet.output.classesDirs).skipWhenEmpty()
        }
    }
}
