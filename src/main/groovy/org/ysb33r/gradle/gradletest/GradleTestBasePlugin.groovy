/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2015 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import org.gradle.api.GradleException
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Task
import org.ysb33r.gradle.gradletest.internal.Jacoco
import org.ysb33r.grolifant.api.core.ProjectOperations

import static org.ysb33r.gradle.gradletest.internal.GradleVersions.GRADLE_4_1_OR_LATER

/** A base plugin for GradleTest. It provides only the ability to add new test sets,
 * but does not create any.
 *
 */
@CompileStatic
class GradleTestBasePlugin implements Plugin<Project> {

    public static final String SUPPORT_URL = 'https://gitlab.com/ysb33rOrg/gradleTest/-/issues'

    @Override
    void apply(Project project) {
        if (!GRADLE_4_1_OR_LATER) {
            throw new GradleException('This plugin is only compatible with Gradle 4.1+')
        }
        project.apply plugin: 'groovy'
        addTestKitTriggers(project)
        addJacocoTriggers(project)

        ProjectOperations.maybeCreateExtension(project)
    }

    /** Adds the appropriate task triggers.
     */
    @CompileDynamic
    private void addTestKitTriggers(Project project) {
        project.extensions.create(GradleTestSets.NAME, GradleTestSets, project)
    }

    private void addJacocoTriggers(Project project) {
        project.pluginManager.withPlugin('jacoco') {
            project.tasks.withType(TestGenerator) { TestGenerator gt ->
                Jacoco.configure(gt)
            }
            project.tasks.whenTaskAdded { Task gt ->
                if (gt instanceof TestGenerator) {
                    Jacoco.configure((TestGenerator) gt)
                }
            }
        }
    }


}
