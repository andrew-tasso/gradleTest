package ${TESTPACKAGE}

import java.util.regex.Pattern
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.FileVisitResult
import java.nio.file.SimpleFileVisitor
import java.nio.file.FileAlreadyExistsException
import java.nio.file.attribute.BasicFileAttributes
import static java.nio.file.StandardCopyOption.COPY_ATTRIBUTES
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING
import static java.nio.file.FileVisitResult.CONTINUE
import org.gradle.testkit.runner.GradleRunner
import org.gradle.util.GradleVersion
import org.gradle.testkit.runner.TaskOutcome
import spock.lang.Specification
import spock.lang.Unroll
import spock.lang.Title

@Title('${TESTNAME} (${LANGUAGE})')
class ${TESTNAME.replaceAll(/\W/,'_')}Spec extends Specification {

    public static final Boolean TESTKIT_PER_TEST = ${TESTKIT_PER_TEST}
    public static final String TESTFILENAME = '${TESTFILENAME}'
    public static final String language = '${LANGUAGE}'
    public static final boolean BUILD_PLUGIN_CLASS_PATH = ${BUILD_PLUGIN_CLASS_PATH }

    public static final File SOURCEDIR = new File('${SOURCEDIR}'.toURI())
    public static final File CP_MANIFEST = BUILD_PLUGIN_CLASS_PATH ? new File('${MANIFEST}'.toURI()) : null
    public static final File WORKDIR = new File('${WORKDIR}'.toURI())
    public static final boolean CHECK_WARNINGS = ${CHECK_WARNINGS}
    public static final boolean CLEAN_CACHEDIR = ${CLEAN_CACHE}
    public static final boolean WITH_DEBUG = ${WITH_DEBUG}
    public static final String CONFIGURATION_CACHE = '${CONFIGURATION_CACHE}'

    public static final String DISTRIBUTION_URI = System.getProperty('org.ysb33r.gradletest.distribution.uri') ?: '${DISTRIBUTION_URI}'
    public static final List<String> gradleArguments = [${ARGUMENTS}]
    public static final List<Pattern> warnings = [
        ~/The .+ method has been deprecated and is scheduled to be removed in Gradle/,
        ~/Gradle now uses separate output directories for each JVM language, but this build assumes a single directory for all classes from a source set/
    ]

    public static List<File> pluginClasspath

    File testProjectDir
    GradleRunner gradleRunner

    void setupSpec() {
        if (!WORKDIR.exists()) {
            WORKDIR.mkdirs()
        }

        if (BUILD_PLUGIN_CLASS_PATH) {
            pluginClasspath = CP_MANIFEST.readLines().collect {
                new File(it)
            }
        }
    }

    @Unroll
    void "${TESTNAME} : Gradle #version using ${LANGUAGE}"() {
        setup:
        final GradleVersion gradleVersionUnderTest = GradleVersion.version(version)
        setupForVersion(version)

        when:
        println 'The build is using ' + GradleVersion.current() + ' and this test project will now use Gradle ' + version
        def result = gradleRunner.${ FAILMODE ? 'buildAndFail' : 'build' }()
        def defaultTaskResult = result.task(':${DEFAULTTASK}')
        List<String> failedTaskNames = result.taskPaths(TaskOutcome.FAILED)

        then:
        ${ FAILMODE ? '!failedTaskNames.empty' : 'defaultTaskResult?.outcome == TaskOutcome.SUCCESS' }
        no_deprecation_warnings(result.output)

        where:
        version << [${ VERSIONS }]
    }

    private void setupForVersion(final String version) {
        final GradleVersion gradleVersionUnderTest = GradleVersion.version(version)
        testProjectDir = new File(WORKDIR, '${TESTBASENAME}/' + version)

        if (CLEAN_CACHEDIR) {
            File cacheDir = new File(testProjectDir, '.gradle')
            if (cacheDir.exists()) {
                cacheDir.deleteDir()
            }
        }

        testProjectDir.mkdirs()
        Path source = SOURCEDIR.toPath()
        Path target = testProjectDir.toPath()

        def ant = new AntBuilder()
        ant.sequential {
            copy(todir: target) {
                fileset(dir: source) {
                    include(name: "**/*")
                }
            }
        }

        final File settings = new File(testProjectDir, 'settings.gradle')
        if (!settings.exists()) {
            settings.text = ''
        }

        setupBuildScriptFile(testProjectDir, TESTFILENAME, language)

        final File deleteNotUsedDslScript = new File(testProjectDir, '${DELETE_SCRIPT}')
        deleteNotUsedDslScript.delete()

        gradleRunner = GradleRunner.create()
            .withProjectDir(testProjectDir)
            .forwardOutput()
            .withDebug(WITH_DEBUG)


        if (TESTKIT_PER_TEST != null) {
            gradleRunner.withTestKitDir(new File(TESTKIT_PER_TEST ? testProjectDir : WORKDIR, 'testkit-data'))
        }

        if (DISTRIBUTION_URI.empty) {
            gradleRunner.withGradleVersion(version)
        } else {
            final String uri = DISTRIBUTION_URI + getDistributionFilename(version)
            gradleRunner.withGradleDistribution(uri.toURI())
        }
        gradleRunner.withArguments(getArguments(gradleVersionUnderTest, version))
        addClasspath(gradleVersionUnderTest, version)
    }

    private void setupBuildScriptFile(File testProjectDir, String testScriptName, String lang) {
        String buildFile
        String stagedBuildFile

        (buildFile, stagedBuildFile) = getScriptFilenames(lang)

        if (testScriptName == buildFile) {
            testScriptName = stagedBuildFile
        }

        File build = new File(testProjectDir, buildFile)
        File staged = new File(testProjectDir, stagedBuildFile)

        if (build.exists() && staged.exists()) {
            // this means that this script has created a staged build file in the past
            // therefore the data in the staged build is the actual build file and the
            // build file is actually the thin wrapper
            build.delete()
        } else if (build.exists() && !staged.exists()) {
            // here we can assume that this has never been run on the folder
            // and the build script is the actual build script
            build.renameTo(staged)
        }

        File testScript = new File(testProjectDir, testScriptName)

        build.write(testScript.text)
    }

    private def getScriptFilenames(String lang) {
        switch (lang) {
            case 'dsl.groovy':
                return ["build.gradle", "staged.build.gradle"]
            case 'dsl.kotlin':
                return ["build.gradle.kts", "staged.build.gradle.kts"]
        }
    }

    private List<String> getArguments(final GradleVersion gradleVersionUnderTest, final String version) {
        List<String> args = gradleArguments + []
        args.addAll('--console=plain', '-Porg.gradle.daemon=false')

        if (gradleVersionUnderTest >= GradleVersion.version('4.5')) {
            args.addAll('--warning-mode=all')
        }

        if(gradleVersionUnderTest >= GradleVersion.version('6.6')) {
            if(CONFIGURATION_CACHE.empty) {
                args.add('--no-configuration-cache')
            } else {
                args.addAll('--configuration-cache',"--configuration-cache-problems=${CONFIGURATION_CACHE}")
            }
        }
        args
    }

    private void addClasspath(final GradleVersion gradleVersionUnderTest, final String version) {
        if (BUILD_PLUGIN_CLASS_PATH) {
            gradleRunner.withPluginClasspath(pluginClasspath)
        } else {
            gradleRunner.withPluginClasspath()
        }
    }

    private def getDistributionFilename(String version) {
        String fname = '${DIST_FILENAME_PATTERN}'
        fname = fname.replaceAll('@version@', version)

        if (!fname.startsWith("/")) {
            fname = "/" + fname
        }

        return fname
    }

    private boolean no_deprecation_warnings(final String output) {
        if (!CHECK_WARNINGS) {
            true
        } else {
            !warnings.any { pat ->
                output =~ pat
            }
        }
    }
}
