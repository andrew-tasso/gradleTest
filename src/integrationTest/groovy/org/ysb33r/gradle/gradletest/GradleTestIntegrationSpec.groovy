/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2015 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.gradle.testkit.runner.GradleRunner
import org.gradle.util.GradleVersion
import org.ysb33r.gradle.gradletest.internal.GradleTestIntegrationSpecification
import spock.lang.Issue
import spock.lang.Unroll

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS
import static org.gradle.testkit.runner.TaskOutcome.UP_TO_DATE

class GradleTestIntegrationSpec extends GradleTestIntegrationSpecification {

    static final List TESTNAMES = ['alpha', 'beta', 'gamma']
    static final List GRADLE_INTEGRATION_VERSIONS = GradleTestIntegrationSpecification.AVAILABLE_GRADLE_VERSIONS
    static
    final File GRADLETESTREPO = new File(System.getProperty('GRADLETESTREPO') ?: 'build/integrationTest/repo').absoluteFile

    @Delegate
    Project project
    File srcDir

    void setup() {
        project = ProjectBuilder.builder().withProjectDir(testProjectDir).build()

        buildFile = new File(project.projectDir, 'build.gradle')
        new File(project.projectDir,'settings.gradle').text = ''
        writeBuildScriptHeader()
        writeGradleTestConfiguration()
     }

    @Unroll
    void "Running with testKitStrategy = #mode"() {

        setup:
        genTestStructureForSuccess(srcDir)

        buildFile << """
        gradleTest.testKitStrategy = gradleTest.${mode}
        """

        when:
        println "I'm starting this test chain and I'm " + GradleVersion.current()
        def result = gradleRunner
            .withArguments('gradleTest', '-i')
            .withDebug(true)
            .build()

        then:
        result.task(":gradleTest").outcome == SUCCESS

        and: "There is a file in the local repo"
        new File("${buildDir}/gradleTest/repo/doxygen-0.2.jar").exists()

        and: "A testkit capture directory was created if strategy is not discardData"
        location == null || new File("${buildDir}/gradleTest/${location}/testkit-data").exists()

        where:
        mode                | location
        'discardData'       | null
        'directoryPerGroup' | '.'
        'directoryPerTest'  | "${TESTNAMES[0]}/${GRADLE_INTEGRATION_VERSIONS[0]}"
    }

    void "Setting up a second test set"() {
        setup:
        buildFile << """
        gradleTestSets.add ('second')

        dependencies {
          secondGradleTest 'org.ysb33r.gradle:doxygen:0.2'
        }

        secondGradleTest {
            versions tasks.gradleTest.versions
            gradleDistributionUri '${GRADLETESTREPO.toURI()}'

            doFirst {
                println 'I am the actual invocation of secondGradleTest (from GradleIntegrationSpec) and I am ' + GradleVersion.current()
            }
        }

"""
        File srcDir2 = new File(project.projectDir, 'src/secondGradleTest')
        srcDir2.mkdirs()
        genTestStructureForSuccess(srcDir2)


        when:
        def result = gradleRunner
            .withArguments('secondGradleTest', '-i')
            .build()

        then:
        result.task(":secondGradleTest").outcome == SUCCESS

        and: "There is a file in the local repo"
        new File("${buildDir}/secondGradleTest/repo/doxygen-0.2.jar").exists()
    }

    @Issue("https://github.com/ysb33r/gradleTest/issues/52")
    @Unroll
    void 'Handle expected failure (using Gradle Version #gradleVer)'() {

        setup:
        buildFile << """
        gradleTest {
            expectFailure ~/gamma/
        }
"""
        genTestStructureForFailure()

        when:
        println "I'm starting this test chain and I'm " + GradleVersion.current()
        def result = gradleRunner
            .withArguments('gradleTest', '-i', '-s')
            .withGradleDistribution(new File(GRADLETESTREPO, "gradle-${gradleVer}-bin.zip").toURI())
            .build()

        then:
        result.task(":gradleTest").outcome == SUCCESS

        where:
        gradleVer << oneOfEachMainGradleReleases()

    }

    @Issue('https://github.com/ysb33r/gradleTest/issues/59')
    def "Adding folders will re-run test task"() {
        setup: "A GradleTest structure with three tests"
        def gradleRunner = gradleRunner.withArguments('gradleTest', '-i')
        genTestStructureForSuccess(srcDir)

        when: 'When executed'
        println "I'm starting this test chain and I'm " + GradleVersion.current()
        def result = gradleRunner.build()

        then: 'The tests execute successfully'
        result.task(":gradleTest").outcome == SUCCESS

        when: 'It is executed again'
        println "I'm running this test chain again and I'm " + GradleVersion.current()
        result = gradleRunner.build()

        then: 'The task should be skipped'
        result.task(":gradleTest").outcome == UP_TO_DATE

        when: 'An additional test is added'
        File testDir = new File(srcDir, 'delta')
        testDir.mkdirs()
        new File(testDir, 'build.gradle').text = '''
            task runGradleTest  {
                ext {
                    gVer = gradle.gradleVersion
                    pName = project.name
                }
                doLast {
                    println "DELTA: I'm runGradleTest and I'm " + gVer
                    println "DELTA: Hello, ${pName}"
                }
            }
'''

        and: 'The task is executed again'
        result = gradleRunner.build()

        then: 'Then the task will be successfully executed'
        result.task(":gradleTest").outcome == SUCCESS
    }

    void genTestStructureForSuccess(File genToDir) {
        TESTNAMES.each {
            File testDir = new File(genToDir, it)
            testDir.mkdirs()
            new File(testDir, 'build.gradle').text = '''
            final String pName = project.name
            final String gVersion = gradle.gradleVersion
            task runGradleTest  {
                doLast {
                    println "I'm the runGradleTest task and I'm being executed from " + gVersion
                    println "Hello, ${pName}"
                }
            }
'''
        }
    }

    void genTestStructureForFailure() {
        TESTNAMES.each {
            File testDir = new File(srcDir, it)
            testDir.mkdirs()
            if (it == 'gamma') {
                new File(testDir, 'build.gradle').text = '''

            task willFail {
                doLast {
                    throw new GradleException('Expect this to fail')
                }
            }
            task runGradleTest  {
                dependsOn willFail
            }
            '''

            } else {
                new File(testDir, 'build.gradle').text = '''
            final String pName = project.name
            final String gVersion = gradle.gradleVersion
            task runGradleTest  {
                doLast {
                    println "I'm the runGradleTest task and I'm being executed from " + gVersion
                    println "Hello, ${pName}"
                }
            }
            '''
            }
        }
    }

    void writeGradleTestConfiguration() {
        String gradleIntegrationVersions = GRADLE_INTEGRATION_VERSIONS.collect { "'${it}'" }.join(', ')
        buildFile << """
        repositories {
            flatDir {
                dirs '${GRADLETESTREPO.toURI()}'.toURI()
            }
        }
        dependencies {
          gradleTest 'org.ysb33r.gradle:doxygen:0.2'
        }
        gradleTest {
            ext {
                gVersion = gradle.gradleVersion
            }
            versions ${gradleIntegrationVersions}
            gradleDistributionUri '${GRADLETESTREPO.toURI()}'

            doFirst {
                println 'I am the actual invocation of GradleTest (from GradleIntegrationSpec) and I am ' + gVersion
            }
        }
        """

        new File(project.projectDir, 'settings.gradle').text = ''
        srcDir = new File(project.projectDir, 'src/gradleTest')
        srcDir.mkdirs()
    }
}