/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2015 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.gradle.testkit.runner.GradleRunner
import org.gradle.testkit.runner.TaskOutcome
import org.ysb33r.gradle.gradletest.internal.GradleTestIntegrationSpecification
import spock.lang.Unroll

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS

class CompilationIntegrationSpec extends GradleTestIntegrationSpecification {

    public static final File GRADLETESTREPO = GradleTestIntegrationSpecification.GRADLETESTREPO

    Project project
    File srcDir

    void setup() {
        project = ProjectBuilder.builder().withProjectDir(testProjectDir).build()
        srcDir = new File(project.projectDir,'src/gradleTest')
        srcDir.mkdirs()
    }

    @Unroll
    def 'Compile generated code using Gradle #gradleVer'() {
        setup:
        configureGradleTest()
        genTestStructure()

        when:
        def result = gradleRunner
            .withArguments(
                'compileGradleTestGroovy',
                'gradleTestClasses',
                'checkTests',
                'checkGroovyVersion',
                'gradleTest',
                '--console=plain',
                '-i',
                '-s'
            )
            .withGradleDistribution(new File(GRADLETESTREPO,"gradle-${gradleVer}-bin.zip").toURI())
            .build()

        then:
        result.task(':gradleTestGenerator').outcome == SUCCESS
        result.task(':compileGradleTestGroovy').outcome == SUCCESS
        result.task(':gradleTestClasses').outcome == SUCCESS
        new File(project.buildDir,"classes${classesLang}/gradleTest/compatibilityTests/dsl/groovylang/AlphaSpec.class").exists()
        result.task(':gradleTest').outcome == SUCCESS

        where:
        gradleVer << oneOfEachMainGradleReleases()
        classesLang = gradleVer.startsWith('3.') ? '' : '/groovy'
    }

    private void genTestStructure() {
        File testDir = new File(srcDir, 'alpha')
        testDir.mkdirs()
        new File(testDir, 'build.gradle').text = '''
            task runGradleTest  {
                doLast {
                }
            }
'''
    }

    private void configureGradleTest() {
        writeBuildScriptHeader()
        buildFile << """

        repositories {
            flatDir {
                dirs '${GRADLETESTREPO.toURI()}'.toURI()
            }
        }

        gradleTest {
            versions ${oneOfEachMainGradleReleasesAsDslString()}
            gradleDistributionUri '${GRADLETESTREPO.toURI()}'
        }
        
        task checkTests {
            doLast {
                FileTree ft
                if(GradleVersion.current() < GradleVersion.version('4.0')) {
                  ft = fileTree(gradleTest.testClassesDir)
                } else {
                  ft = gradleTest.testClassesDirs.asFileTree               
                } 
                if(ft.files.empty) {
                  throw GradleException('No classes available to test')                 
                }
            }
         }
         
         task checkGroovyVersion {   
            doLast {
                GradleVersion current = GradleVersion.current()
                GradleVersion g5 = GradleVersion.version('5.0')
                GradleVersion g7 = GradleVersion.version('7.0')
                String configToCheck = 'gradleTestRuntimeClasspath'
                String verToCheck = current < g5 ? '2.4' : (current < g7 ? '2.5' : '3.0')
                
                def artifacts = configurations.getByName(configToCheck).files.findAll {
                    if(current < g7) {
                        it.name.startsWith('groovy-all')
                    } else {
                        it.name.startsWith('groovy-3')
                    }
                }
                
                if(artifacts.size() != 1) {
                    throw GradleException( 'Found more than one groovy-all on classpath: ' + artifacts.toString())
                }
                
                if(!artifacts.first().name.contains( verToCheck )) {
                    throw GradleException( 'Groovy version (' + artifacts.first().name + ') is not of the ' + verToCheck + ' family.')
                }
            }
        }
"""
    }
}