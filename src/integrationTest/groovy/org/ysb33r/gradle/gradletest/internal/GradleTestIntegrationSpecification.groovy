/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2015 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest.internal

import org.gradle.testkit.runner.GradleRunner
import org.ysb33r.grolifant.api.core.Version
import spock.lang.Specification
import spock.lang.TempDir

/**
 * Before running anything from here, ensure that `gradle createIntegrationTestClasspathManifest` has been run.
 */
class GradleTestIntegrationSpecification extends Specification {

    // Always keep this list on one line - it is read in the build
    public static final List AVAILABLE_GRADLE_VERSIONS = ['4.1', '4.10.3', '5.6.3', '6.0.1', '6.8.3', '7.0']

    public static final File GRADLETESTREPO = new File(System.getProperty('GRADLETESTREPO') ?: 'build/integrationTest/repo').absoluteFile
    public static final String OFFLINE_REPO_DSL = new File(System.getProperty('OFFLINE_REPO_DSL')).text

    @TempDir
    File testProjectDir

    File buildFile
    File projectDir
    File buildDir
    File settingsFile

    void setup() {
        projectDir = testProjectDir
        buildFile = new File(projectDir, 'build.gradle')
        buildDir = new File(projectDir, 'build')
        settingsFile = new File(projectDir, 'settings.gradle')
        settingsFile.text = 'rootProject.name = "integration-test-project"'
    }

    void writeBuildScriptHeader() {
        buildFile << """
            import org.gradle.util.GradleVersion 

            plugins {
                id 'java'
                id 'org.ysb33r.gradletest'
            }
            
            ${OFFLINE_REPO_DSL}
"""
    }

    GradleRunner getGradleRunner() {
        GradleRunner.create()
            .withProjectDir(projectDir)
            .withPluginClasspath()
            .forwardOutput()
    }

    static List<String> allAvailableGradleReleases() {
        AVAILABLE_GRADLE_VERSIONS
    }

    static List<String> oneOfEachMainGradleReleases() {
        AVAILABLE_GRADLE_VERSIONS.groupBy {
            Version.of(it).major
        }.values()*.getAt(0)
    }

    static String oneOfEachMainGradleReleasesAsDslString(final String quoteCharacter = '\'') {
        oneOfEachMainGradleReleases().collect {
            "${quoteCharacter}${it}${quoteCharacter}"
        }.join(',')
    }
}
